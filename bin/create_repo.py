#!/usr/bin/env tdaq_python
#
# create_repo.py output.data.xml input.yaml [ other includes ]
#
import sys
import config
import yaml

import argparse

parser = argparse.ArgumentParser(description='Create a TDAQ software repository')
parser.add_argument('repo')
parser.add_argument('output_file')
parser.add_argument('input_file')
parser.add_argument('includes',nargs='*')
args = parser.parse_args()

conf = config.Configuration()
conf.create_db(args.output_file, ['daq/schema/core.schema.xml'] + args.includes)

repo = yaml.load(open(args.input_file), Loader=yaml.FullLoader)

is_info_files = repo['IS_INFO_FILES']
del repo['IS_INFO_FILES']
igui_properties = repo['IGUI_PROPERTIES']
del repo['IGUI_PROPERTIES']

tags = [ t for t in conf.get_dals('Tag') if t.className() == 'Tag' ]
sw_objects = []
sw_repo = None

def get_uses(uses):
    result = []
    for pkg in uses:
        repo = None
        try:
            repo = conf.get_dal('SW_Repository', pkg)
        except:
            try:
              repo = conf.get_dal('SW_ExternalPackage', pkg)
            except:
              pass
        if repo: result.append(repo)
    return result

# Step 0: Create all objects
for obj in repo:
    name, Class = obj.split('@',1)
    if Class in [ 'Binary', 'Script', 'JarFile' ] or (Class == 'SW_Repository' and name == args.repo):
        conf.create_obj(Class, name)

# Step 1: Fill attributes and resolve relations
for obj in repo:
    name, Class = obj.split('@',1)

    # The configuration
    c = repo[obj]

    # The dal object
    if Class in [ 'Binary', 'Script', 'JarFile' ] or (Class == 'SW_Repository' and name == args.repo):
        d = conf.get_dal(Class, name)
    else:
        continue

    # Variable
    if Class == 'Variable':
        d.Name = c.get('Name',name)
        d.Value = c.get('Value','')
        d.Description = c.get('Description','')
    # SW_PackageVariable
    elif Class == 'SW_PackageVariable':
        d.Name = name
        d.Suffix = c.get('Suffix','')
        d.Description = c.get('Description','')
    elif Class == 'SW_ExternalPackage':
        d.InstallationPath = c['InstallationPath']
        d.Name = c.get('Name', name)
        for mapping in c.get('SharedLibraries',[]):
            m = conf.get_dal('TagMapping', mapping)
            d.SharedLibraries.append(m)
        for mapping in c.get('Binaries',[]):
            m = conf.get_dal('TagMapping', mapping)
            d.Binaries.append(m)
        for p in c.get('Uses',[]):
            other = conf.get_dal('SW_ExternalPackage', p)
            d.Uses.append(other)
        for e in c.get('ProcessEnvironment',[]):
            v = conf.get_dal('Variable', e)
            d.ProcessEnvironment.append(v)
    elif Class == 'Binary' or Class == 'Script':
        d.BinaryName = c['BinaryName']
        d.Description = c.get('Description','')
        d.DefaultParameters = c.get('Parameters','')
        d.HelpURL = c.get('HelpURL','')
        d.Needs = [ conf.get_dal('RM_HW_Resource',rsrc) for rsrc in c.get('Resources',[])]
        if 'Shell' in c:
            d.Shell = c['Shell']
        for e in c.get('Environment',[]):
            v = conf.get_dal('Variable', e)
            d.ProcessEnvironment.append(v)
        d.Uses = get_uses(c.get('Uses',[]))
        sw_repo = c.get('BelongsTo', None)
        if sw_repo:
          d.BelongsTo = conf.get_dal('SW_Repository', sw_repo)
        else:
          sw_objects.append(d)
    elif Class == 'JarFile':
        d.BinaryName = c['BinaryName']
        d.Description = c.get('Description','')
        sw_objects.append(d)
    elif Class == 'TagMapping':
        d.HW_Tag = c['HW_Tag']
        d.SW_Tag = c['SW_Tag']
        d.Value =  c.get('Value','')
    elif Class == 'SW_Repository':
        d.Name = c.get('Name','')
        d.InstallationPath = c.get('InstallationPath','')
        d.InstallationPathVariableName = c.get('InstallationPathVariableName','')
        d.PatchArea = c.get('PatchArea','')
        d.Tags = tags
        d.Uses = get_uses(c.get('Uses',[]))
        d.ProcessEnvironment = [ conf.get_dal('Parameter', name) for name in c.get('ProcessEnvironment', []) ]
        d.PythonPaths = c.get('PythonPath', [])

    conf.update_dal(d)

sw_repo = conf.get_dal('SW_Repository', args.repo)
for obj in sw_objects:
    obj.BelongsTo = sw_repo
    conf.update_dal(obj)
sw_repo.SW_Objects = sw_objects
if is_info_files:
    sw_repo.ISInfoDescriptionFiles = is_info_files
if igui_properties:
    sw_repo.IGUIProperties = igui_properties
conf.update_dal(sw_repo)

conf.commit()
