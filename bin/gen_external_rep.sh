#!/bin/sh

packages="$1"
packages2="`echo $1 | sed 's/ /\\\|/g'`"
#echo External packages: $packages

for p in $packages; do
#echo Package $p:
#echo "macro sw.environment.variable `cmt show macro_value ${p}_home`"
echo "macro  sw.external.package.$p:lib.mapping `cmt show macro_value ${p}_libdir`"
echo "macro  sw.external.package.$p:bin.mapping `cmt show macro_value ${p}_bindir`"
echo "macro  sw.external.package.$p:cmt.tag ${CMTCONFIG}"
echo "macro  sw.external.package.$p:installation.path `cmt -quiet show macro ${p}_instpath | grep -v '^#' | sed 's/^.*=//'`"
for e in `cmt -quiet show macro ${p}_needsenvironment | grep -v '^#' | sed 's/^.*=//' | sed "s/'//g"` ; do
  if [ "'" != "$e" ] ; then echo "macro  sw.external.package.$p:needs.environment '$e'" ; fi
done

variables=`cmt -quiet show macro_value ${p}_usevariables 2>/dev/null`
#echo $p needs varaibles: ${variables}
 for v in $variables; do
#   echo macro sw.environment.variable.`cmt -quiet show set $v 2>/dev/null | grep -v "^#" | sed 's/=/:value /'`
   echo macro sw.environment.variable.$v:value `cmt -quiet show macro_value $v 2>/dev/null`
 done
#uses=`cmt -quiet broadcast -select=$p cmt -quiet show uses | grep -v "^#"`
uses=`cmt -quiet broadcast -global -select=$p "cmt -quiet show uses" | grep -v "^#" | \
awk '{ print $2 }' | grep $packages2`
  for f in $uses; do
    echo "macro sw.external.package.$p:uses $f"
  done
done #packages
