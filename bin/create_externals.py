#!/usr/bin/env tdaq_python
#
# create_externals.py repo_name output.data.xml input.yaml [ other includes ]
#
import sys
import string
import config
import yaml

from pprint import pprint

import argparse

parser = argparse.ArgumentParser(description='Create externals for TDAQ software repository')
parser.add_argument('repo')
parser.add_argument('output_file')
parser.add_argument('input_file')
parser.add_argument('includes',nargs='*')
args = parser.parse_args()

conf = config.Configuration()
conf.create_db(args.output_file, ['daq/schema/core.schema.xml'] + args.includes)

def get_uses(uses):
    result = []
    for pkg in uses:
        repo = None
        try:
            repo = conf.get_dal('SW_Repository', pkg)
        except:
            try:
              repo = conf.get_dal('SW_ExternalPackage', pkg)
            except:
              pass
        if repo: result.append(repo)
    return result

repo = yaml.load(open(args.input_file), Loader=yaml.FullLoader)

is_info_files = repo['IS_INFO_FILES']
del repo['IS_INFO_FILES']
igui_properties = repo['IGUI_PROPERTIES']
del repo['IGUI_PROPERTIES']

tags = [ t for t in conf.get_dals('Tag') if t.className() == 'Tag' ]
sw_objects = []

# Step 0: Create all objects
for obj in repo:
    name, Class = obj.split('@',1)
    if Class in [ 'Binary', 'Script', 'JarFile' ]: continue
    if Class != 'SW_Repository' or (Class == 'SW_Repository' and name != args.repo):
        conf.create_obj(Class, name)

# Step 1: Fill attributes and resolve relations
for obj in repo:
    name, Class = obj.split('@',1) 

    if Class in [ 'Binary', 'Script', 'JarFile' ] or (Class == 'SW_Repository' and name == args.repo):         continue

    # The configuration
    c = repo[obj]

    # The dal object
    d = conf.get_dal(Class, name)

    # Variable
    if Class == 'Variable':
        d.Name = c.get('Name',name)
        d.Value = c.get('Value','')
        d.Description = c.get('Description','')
        for mapping in c.get('TagMapping',[]):
            try:
                tm = conf.get_dal('TagMapping', mapping)
                d.TagValues.append(tm)
            except Exception as ex:
                # assume default tag mappings, ugly hack
                t = mapping.split('/')[-1]
                tm = conf.create_obj('TagMapping', t+'-for-'+name)
                tm = conf.get_dal('TagMapping',t+'-for-'+name)
                tm.HW_Tag = '-'.join(t.split('-')[0:2])
                tm.SW_Tag = '-'.join(t.split('-')[2:])
                tm.Value = d.Value + '/' + t
                conf.update_dal(tm)
                d.TagValues.append(tm)
    # SW_PackageVariable
    elif Class == 'SW_PackageVariable':
        d.Name = name
        d.Suffix = c.get('Suffix','')
        d.Description = c.get('Description','')
    elif Class == 'SW_ExternalPackage':
        d.InstallationPath = c['InstallationPath']
        d.PatchArea = c.get('PatchArea','')
        d.Name = c.get('Name', name)
        for mapping in c.get('SharedLibraries',[]):
            m = conf.get_dal('TagMapping', mapping)
            d.SharedLibraries.append(m)
        for mapping in c.get('Binaries',[]):
            m = conf.get_dal('TagMapping', mapping)
            d.Binaries.append(m)
        for p in c.get('Uses',[]):
            other = conf.get_dal('SW_ExternalPackage', p)
            d.Uses.append(other)
        for e in c.get('ProcessEnvironment',[]):
            v = conf.get_dal('Variable', e)
            d.ProcessEnvironment.append(v)
    elif Class == 'TagMapping':
        d.HW_Tag = c['HW_Tag']
        d.SW_Tag = c['SW_Tag']
        d.Value =  c.get('Value','')
    elif Class == 'SW_Repository':
        d.Name = c.get('Name','')
        d.InstallationPath = c.get('InstallationPath','')
        d.PatchArea = c.get('PatchArea','')
        d.Tags = tags
        d.Uses = [ get_uses(pkg) for pkg in c.get('Uses',[]) ]
        d.ProcessEnvironment = [ conf.get_dal('Parameter', name) for name in c.get('ProcessEnvironment', []) ]
        d.PythonPaths = c.get('PythonPath', [])

    conf.update_dal(d)

conf.commit()
