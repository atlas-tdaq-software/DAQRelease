#!/usr/bin/env tdaq_python

import sys
import config

conf = config.Configuration()
conf.create_db('tags.data.xml', ['daq/schema/core.schema.xml'])

for tag in sys.argv[1:]:
    arch,os,cxx,opt = tag.split('-')

    conf.create_obj('Tag', tag)
    t = conf.get_dal('Tag', tag)
    t.HW_Tag = arch + '-' + os
    t.SW_Tag = cxx + '-'  + opt
    conf.update_dal(t)

    conf.create_obj('TagMapping', tag+'-for-'+tag + '/bin')
    tm = conf.get_dal('TagMapping', tag+'-for-'+tag + '/bin')
    tm.HW_Tag = arch + '-' + os
    tm.SW_Tag = cxx + '-'  + opt
    tm.Value = tag + '/bin'
    conf.update_dal(tm)

    conf.create_obj('TagMapping', tag+'-for-'+tag + '/lib')
    tm = conf.get_dal('TagMapping', tag+'-for-'+tag + '/lib')
    tm.HW_Tag = arch + '-' + os
    tm.SW_Tag = cxx + '-'  + opt
    tm.Value = tag + '/lib'
    conf.update_dal(tm)

    conf.create_obj('TagMapping', tag+'-for-'+tag + '/lib64')
    tm = conf.get_dal('TagMapping', tag+'-for-'+tag + '/lib64')
    tm.HW_Tag = arch + '-' + os
    tm.SW_Tag = cxx + '-'  + opt
    tm.Value = tag + '/lib64'
    conf.update_dal(tm)

conf.commit()

