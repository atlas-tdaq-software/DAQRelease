/**
\mainpage (DAQ/HLTI Software Documentation: doxygen for release packages)

DAQ/HLTI Software Release contains packages used for running the ATLAS Data Acquisition System (DAQ).

It is comprised of the Data Flow, Configuration, Controls and Monitoring Systems. The High-Level Trigger (HLT) infrastructure comprises the dataflow and control software for the LVL2 and EF trigger systems - collectively known as the HLT. 

[Twiki pages](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/HltDaqMainPage)

[Release notes](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/download/tdaq-04-00-01/RELEASE_NOTES.html)
*/
